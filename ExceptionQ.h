#ifndef EXCEPTIONQ_H
#define EXCEPTIONQ_H

#include <exception>
#include <string>

class ExceptionQ: public std::exception {
    std::string message;

public:
    ExceptionQ(const int& chose);

    virtual const char* what() const throw();
};

#endif //EXCEPTIONQ