#include <iostream>
#include <cmath>
#include <vector>


using namespace std;

template <typename T, typename T1 >
auto add1(T x, T1 y) {

    return x + y;
}

template <typename T, typename T1 >
auto add1(T *x, T1 *y) {

    return x + *y ;
};

template<>
auto add1 (const char * x,const char * y){
    string temp = x ;
    return temp + y;
};



int main() {


//zad 1
    std::cout << endl;
    std::cout << "zad1" << endl;
    auto a =7 , b= 5, *c = &a , *d = &b ;
    std::cout << add1(c,d) << endl;

//zad2
    std::cout << endl;
    std::cout << "zad2" << endl;
    const char *e = "wo";
    const char *f = "da";


    std::cout <<  add1(e,f) << endl;



    return 0;
}