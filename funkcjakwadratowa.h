//
// Created by magu on 30.10.18.
//

#ifndef CPP_FUNKCJAKWADRATOWA_H
#define CPP_FUNKCJAKWADRATOWA_H


class funkcjaKwadratowa {
private:
    double a, b, c;


public:
    funkcjaKwadratowa(double _a, double _b, double _c);

    double oblicz(unsigned int chose);


};


#endif //CPP_FUNKCJAKWADRATOWA_H
